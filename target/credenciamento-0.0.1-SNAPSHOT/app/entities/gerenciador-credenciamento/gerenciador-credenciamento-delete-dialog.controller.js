(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .controller('GerenciadorCredenciamentoDeleteController',GerenciadorCredenciamentoDeleteController);

    GerenciadorCredenciamentoDeleteController.$inject = ['$uibModalInstance', 'entity', 'GerenciadorCredenciamento'];

    function GerenciadorCredenciamentoDeleteController($uibModalInstance, entity, GerenciadorCredenciamento) {
        var vm = this;

        vm.gerenciadorCredenciamento = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            GerenciadorCredenciamento.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
