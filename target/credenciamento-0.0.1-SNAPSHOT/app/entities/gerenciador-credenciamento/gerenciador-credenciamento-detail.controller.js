(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .controller('GerenciadorCredenciamentoDetailController', GerenciadorCredenciamentoDetailController);

    GerenciadorCredenciamentoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'GerenciadorCredenciamento'];

    function GerenciadorCredenciamentoDetailController($scope, $rootScope, $stateParams, previousState, entity, GerenciadorCredenciamento) {
        var vm = this;

        vm.gerenciadorCredenciamento = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('credenciamentoApp:gerenciadorCredenciamentoUpdate', function(event, result) {
            vm.gerenciadorCredenciamento = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
