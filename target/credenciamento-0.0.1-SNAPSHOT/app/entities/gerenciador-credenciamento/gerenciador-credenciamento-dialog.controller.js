(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .controller('GerenciadorCredenciamentoDialogController', GerenciadorCredenciamentoDialogController);

    GerenciadorCredenciamentoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'GerenciadorCredenciamento'];

    function GerenciadorCredenciamentoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, GerenciadorCredenciamento) {
        var vm = this;

        vm.gerenciadorCredenciamento = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.gerenciadorCredenciamento.id !== null) {
                GerenciadorCredenciamento.update(vm.gerenciadorCredenciamento, onSaveSuccess, onSaveError);
            } else {
                GerenciadorCredenciamento.save(vm.gerenciadorCredenciamento, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('credenciamentoApp:gerenciadorCredenciamentoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
