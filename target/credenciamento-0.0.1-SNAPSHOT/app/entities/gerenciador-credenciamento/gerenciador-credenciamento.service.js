(function() {
    'use strict';
    angular
        .module('credenciamentoApp')
        .factory('GerenciadorCredenciamento', GerenciadorCredenciamento);

    GerenciadorCredenciamento.$inject = ['$resource'];

    function GerenciadorCredenciamento ($resource) {
        var resourceUrl =  'api/credenciamentos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
