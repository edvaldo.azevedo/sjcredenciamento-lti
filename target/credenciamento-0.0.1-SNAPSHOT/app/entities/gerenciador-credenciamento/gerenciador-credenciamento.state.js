(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('gerenciador-credenciamento', {
            parent: 'entity',
            url: '/gerenciador-credenciamento',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'credenciamentoApp.gerenciadorCredenciamento.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/gerenciador-credenciamento/gerenciador-credenciamentos.html',
                    controller: 'GerenciadorCredenciamentoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('gerenciadorCredenciamento');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('gerenciador-credenciamento-detail', {
            parent: 'gerenciador-credenciamento',
            url: '/gerenciador-credenciamento/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'credenciamentoApp.gerenciadorCredenciamento.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/gerenciador-credenciamento/gerenciador-credenciamento-detail.html',
                    controller: 'GerenciadorCredenciamentoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('gerenciadorCredenciamento');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'GerenciadorCredenciamento', function($stateParams, GerenciadorCredenciamento) {
                    return GerenciadorCredenciamento.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'gerenciador-credenciamento',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('gerenciador-credenciamento-detail.edit', {
            parent: 'gerenciador-credenciamento-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/gerenciador-credenciamento/gerenciador-credenciamento-dialog.html',
                    controller: 'GerenciadorCredenciamentoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GerenciadorCredenciamento', function(GerenciadorCredenciamento) {
                            return GerenciadorCredenciamento.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('gerenciador-credenciamento.new', {
            parent: 'gerenciador-credenciamento',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/gerenciador-credenciamento/gerenciador-credenciamento-dialog.html',
                    controller: 'GerenciadorCredenciamentoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('gerenciador-credenciamento', null, { reload: 'gerenciador-credenciamento' });
                }, function() {
                    $state.go('gerenciador-credenciamento');
                });
            }]
        })
        .state('gerenciador-credenciamento.edit', {
            parent: 'gerenciador-credenciamento',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/gerenciador-credenciamento/gerenciador-credenciamento-dialog.html',
                    controller: 'GerenciadorCredenciamentoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GerenciadorCredenciamento', function(GerenciadorCredenciamento) {
                            return GerenciadorCredenciamento.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('gerenciador-credenciamento', null, { reload: 'gerenciador-credenciamento' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('gerenciador-credenciamento.delete', {
            parent: 'gerenciador-credenciamento',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/gerenciador-credenciamento/gerenciador-credenciamento-delete-dialog.html',
                    controller: 'GerenciadorCredenciamentoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['GerenciadorCredenciamento', function(GerenciadorCredenciamento) {
                            return GerenciadorCredenciamento.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('gerenciador-credenciamento', null, { reload: 'gerenciador-credenciamento' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
