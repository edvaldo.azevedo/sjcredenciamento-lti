(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('credenciamento', {
            parent: 'entity',
            url: '/credenciamento',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'credenciamentoApp.credenciamento.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/credenciamento/credenciamentos.html',
                    controller: 'CredenciamentoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('credenciamento');
                    $translatePartialLoader.addPart('statusCredenciamento');
                    $translatePartialLoader.addPart('tipoEmpresa');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('form',{
             url: '/form',
             onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/credenciamento/credenciamento-dialog.html',
                    controller: 'CredenciamentoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nomeCompleto: null,
                                nomeResumido: null,
                                email: null,
                                funcao: null,
                                statusCredenciamento: null,
                                cep: null,
                                rua: null,
                                numero: null,
                                complemento: null,
                                cidade: null,
                                uf: null,
                                telefoneResidencial: null,
                                celular: null,
                                whatsApp: null,
                                nomeEmpresa: null,
                                cnpj: null,
                                cepEmpresa: null,
                                enderecoSede: null,
                                telefoneSede: null,
                                tipoEmpresa: null,
                                emailInstitucional: null,
                                gerenteSetorOuProfissionalResponsavel: null,
                                telefone: null,
                                identidade: null,
                                orgaoEmissor: null,
                                cpf: null,
                                drt: null,
                                foto: null,
                                fotoContentType: null,
                                id: null
                            };
                        }
                    }
                });
            }]
        })
        .state('credenciamento-detail', {
            parent: 'credenciamento',
            url: '/credenciamento/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'credenciamentoApp.credenciamento.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/credenciamento/credenciamento-detail.html',
                    controller: 'CredenciamentoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('credenciamento');
                    $translatePartialLoader.addPart('statusCredenciamento');
                    $translatePartialLoader.addPart('tipoEmpresa');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Credenciamento', function($stateParams, Credenciamento) {
                    return Credenciamento.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'credenciamento',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('credenciamento-detail.edit', {
            parent: 'credenciamento-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/credenciamento/credenciamento-dialog.html',
                    controller: 'CredenciamentoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Credenciamento', function(Credenciamento) {
                            return Credenciamento.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('credenciamento.new', {
            url: '/new',
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/credenciamento/credenciamento-dialog.html',
                    controller: 'CredenciamentoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nomeCompleto: null,
                                nomeResumido: null,
                                email: null,
                                funcao: null,
                                statusCredenciamento: null,
                                cep: null,
                                rua: null,
                                numero: null,
                                complemento: null,
                                cidade: null,
                                uf: null,
                                telefoneResidencial: null,
                                celular: null,
                                whatsApp: null,
                                nomeEmpresa: null,
                                cnpj: null,
                                cepEmpresa: null,
                                enderecoSede: null,
                                telefoneSede: null,
                                tipoEmpresa: null,
                                emailInstitucional: null,
                                gerenteSetorOuProfissionalResponsavel: null,
                                telefone: null,
                                identidade: null,
                                orgaoEmissor: null,
                                cpf: null,
                                drt: null,
                                foto: null,
                                fotoContentType: null,
                                id: null
                            };
                        }
                    }
                });
            }]
        })
        .state('credenciamento.edit', {
            parent: 'credenciamento',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/credenciamento/credenciamento-dialog.html',
                    controller: 'CredenciamentoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Credenciamento', function(Credenciamento) {
                            return Credenciamento.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('credenciamento', null, { reload: 'credenciamento' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('credenciamento.delete', {
            parent: 'credenciamento',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/credenciamento/credenciamento-delete-dialog.html',
                    controller: 'CredenciamentoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Credenciamento', function(Credenciamento) {
                            return Credenciamento.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('credenciamento', null, { reload: 'credenciamento' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
