(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .controller('CredenciamentoDialogController', CredenciamentoDialogController);

    CredenciamentoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Credenciamento','$state','ngToast'];

    function CredenciamentoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, Credenciamento,$state,ngToast) {
        var vm = this;

        vm.credenciamento = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
            $state.go('home');
        }

        function save () {
            vm.isSaving = true;
            if (vm.credenciamento.id !== null) {
                Credenciamento.update(vm.credenciamento, onSaveSuccess, onSaveError);
            } else {
                Credenciamento.save(vm.credenciamento, onSaveSuccess, onSaveError);
            }
            $state.go('home');
        }

        function onSaveSuccess (result) {
            $scope.$emit('credenciamentoApp:credenciamentoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
            ngToast.create({
                className: 'success',
                content: '<a href="#" class="">Sua solicitação foi enviada com sucesso.</a>'
            });
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setFoto = function ($file, credenciamento) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        credenciamento.foto = base64Data;
                        credenciamento.fotoContentType = $file.type;
                    });
                });
            }
        };

    }
})();
