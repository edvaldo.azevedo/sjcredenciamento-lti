(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .controller('CredenciamentoController', CredenciamentoController);

    CredenciamentoController.$inject = ['DataUtils', 'Credenciamento'];

    function CredenciamentoController(DataUtils, Credenciamento) {

        var vm = this;

        vm.credenciamentos = [];
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;

        loadAll();

        function loadAll() {
            Credenciamento.query(function(result) {
                vm.credenciamentos = result;
                vm.searchQuery = null;
            });
        }
    }
})();
