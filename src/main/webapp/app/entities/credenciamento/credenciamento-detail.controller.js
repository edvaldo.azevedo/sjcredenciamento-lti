(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .controller('CredenciamentoDetailController', CredenciamentoDetailController);

    CredenciamentoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Credenciamento'];

    function CredenciamentoDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Credenciamento) {
        var vm = this;

        vm.credenciamento = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('credenciamentoApp:credenciamentoUpdate', function(event, result) {
            vm.credenciamento = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
