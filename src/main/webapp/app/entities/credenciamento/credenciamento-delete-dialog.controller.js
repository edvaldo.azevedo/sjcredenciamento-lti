(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .controller('CredenciamentoDeleteController',CredenciamentoDeleteController);

    CredenciamentoDeleteController.$inject = ['$uibModalInstance', 'entity', 'Credenciamento'];

    function CredenciamentoDeleteController($uibModalInstance, entity, Credenciamento) {
        var vm = this;

        vm.credenciamento = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Credenciamento.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
