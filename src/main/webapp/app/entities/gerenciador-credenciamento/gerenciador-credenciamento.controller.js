(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .controller('GerenciadorCredenciamentoController', GerenciadorCredenciamentoController);

    GerenciadorCredenciamentoController.$inject = ['GerenciadorCredenciamento'];

    function GerenciadorCredenciamentoController(GerenciadorCredenciamento) {

        var vm = this;
        vm.credenciamento = {};
        vm.gerenciadorCredenciamentos = [];

        loadAll();

        function loadAll() {
            GerenciadorCredenciamento.query(function(result) {
                vm.gerenciadorCredenciamentos = result;
                vm.searchQuery = null;
            });
        }
        vm.aprovarCredenciamento = function(index){
            vm.gerenciadorCredenciamentos[index].statusCredenciamento = 'APROVADO';
            vm.credenciamento = vm.gerenciadorCredenciamentos[index];
            GerenciadorCredenciamento.update(vm.credenciamento);
        }
        vm.reprovarCredenciamento = function(index){
            vm.gerenciadorCredenciamentos[index].statusCredenciamento = 'NAO_APROVADO';
            vm.credenciamento = vm.gerenciadorCredenciamentos[index];
            GerenciadorCredenciamento.update(vm.credenciamento);
        }
        vm.analisarCredenciamento = function(index){
             vm.gerenciadorCredenciamentos[index].statusCredenciamento = 'EM_ANALISE';
             vm.credenciamento = vm.gerenciadorCredenciamentos[index];
            GerenciadorCredenciamento.update(vm.credenciamento);
        }
        
    }
})();
