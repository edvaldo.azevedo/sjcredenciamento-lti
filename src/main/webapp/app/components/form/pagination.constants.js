(function() {
    'use strict';

    angular
        .module('credenciamentoApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
