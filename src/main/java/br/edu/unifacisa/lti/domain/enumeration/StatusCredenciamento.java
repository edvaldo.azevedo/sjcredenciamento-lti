package br.edu.unifacisa.lti.domain.enumeration;

/**
 * The StatusCredenciamento enumeration.
 */
public enum StatusCredenciamento {
    EM_ANALISE,APROVADO,NAO_APROVADO
}
