package br.edu.unifacisa.lti.domain;

import java.security.GeneralSecurityException;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import com.sun.mail.util.MailSSLSocketFactory;

public class EnviadorEmail {
	private static String HOST = "smtp.gmail.com";
	private static String USER = "credenciamento.sjcg@gmail.com";
	private static String PASSWORD = "campina12!";
	private static String PORT = "465";
	private static String FROM = "credenciamento.sjcg@gmail.com";
	private static String STARTTLS = "true";
	private static String AUTH = "true";
	private static String DEBUG = "true";
	private static String PROTOCOL = "smtp";

	private static String SOCKET_FACTORY = "javax.net.ssl.SSLSocketFactory";

	public void send(String emailDestino, String assunto, String mensagem) throws GeneralSecurityException {
		// Use Properties object to set environment properties
		Properties props = new Properties();

		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.port", PORT);
		props.put("mail.smtp.user", USER);
		MailSSLSocketFactory sf = new MailSSLSocketFactory();
		sf.setTrustAllHosts(true);
		props.put("mail.imap.ssl.trust", "*");
		props.put("mail.imap.ssl.socketFactory", sf);
		props.setProperty("mail.smtp.ssl.trust", HOST);
		props.put("mail.smtp.auth", AUTH);
		props.put("mail.smtp.ssl.enable", "true");
		props.put("mail.smtp.starttls.enable", STARTTLS);
		props.put("mail.transport.protocol", PROTOCOL);
		props.put("mail.smtp.debug", DEBUG);
		props.put("mail.smtp.socketFactory.port", PORT);
		props.put("mail.smtp.socketFactory.class", SOCKET_FACTORY);
		props.put("mail.smtp.socketFactory.fallback", "false");

		try {

			// Obtain the default mail session
			Session session = Session.getDefaultInstance(props, null);
			session.setDebug(true);

			// Construct the mail message
			MimeMessage message = new MimeMessage(session);
			message.setText(mensagem);
			message.setSubject(assunto);
			message.setFrom(new InternetAddress(FROM));
			message.addRecipient(RecipientType.TO, new InternetAddress(emailDestino));
			message.saveChanges();

			// Use Transport to deliver the message
			Transport transport = session.getTransport(PROTOCOL);
			transport.connect(HOST, USER, PASSWORD);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
