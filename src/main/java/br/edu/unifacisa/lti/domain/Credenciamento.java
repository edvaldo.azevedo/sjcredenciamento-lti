package br.edu.unifacisa.lti.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import br.edu.unifacisa.lti.domain.enumeration.StatusCredenciamento;

import br.edu.unifacisa.lti.domain.enumeration.TipoEmpresa;

/**
 * A Credenciamento.
 */
@Entity
@Table(name = "credenciamento")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Credenciamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nome_completo", nullable = false)
    private String nomeCompleto;

    @NotNull
    @Column(name = "nome_resumido", nullable = false)
    private String nomeResumido;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "funcao", nullable = false)
    private String funcao;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_credenciamento")
    private StatusCredenciamento statusCredenciamento;

    @NotNull
    @Column(name = "cep", nullable = false)
    private String cep;

    @NotNull
    @Column(name = "rua", nullable = false)
    private String rua;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @Column(name = "complemento")
    private String complemento;

    @NotNull
    @Column(name = "cidade", nullable = false)
    private String cidade;

    @NotNull
    @Column(name = "uf", nullable = false)
    private String uf;

   
    @Column(name = "telefone_residencial", nullable = true)
    private String telefoneResidencial;

    @NotNull
    @Column(name = "celular", nullable = false)
    private String celular;

    
    @Column(name = "whats_app", nullable = true)
    private String whatsApp;

    @NotNull
    @Column(name = "nome_empresa", nullable = false)
    private String nomeEmpresa;

    @NotNull
    @Column(name = "cnpj", nullable = false)
    private String cnpj;

    @NotNull
    @Column(name = "cep_empresa", nullable = false)
    private String cepEmpresa;

    @NotNull
    @Column(name = "endereco_sede", nullable = false)
    private String enderecoSede;

    @NotNull
    @Column(name = "telefone_sede", nullable = false)
    private String telefoneSede;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_empresa", nullable = false)
    private TipoEmpresa tipoEmpresa;

    @NotNull
    @Column(name = "email_institucional", nullable = false)
    private String emailInstitucional;

    @NotNull
    @Column(name = "gerente_setor_ou_profissional_responsavel", nullable = false)
    private String gerenteSetorOuProfissionalResponsavel;

    @NotNull
    @Column(name = "telefone", nullable = false)
    private String telefone;

    @NotNull
    @Column(name = "identidade", nullable = false)
    private String identidade;

    @NotNull
    @Column(name = "orgao_emissor", nullable = false)
    private String orgaoEmissor;

    @NotNull
    @Column(name = "cpf", nullable = false)
    private String cpf;

    @Column(name = "drt", nullable = true)
    private String drt;

    @NotNull
    @Lob
    @Column(name = "foto", nullable = false)
    private byte[] foto;

    @Column(name = "foto_content_type", nullable = false)
    private String fotoContentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public Credenciamento nomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
        return this;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getNomeResumido() {
        return nomeResumido;
    }

    public Credenciamento nomeResumido(String nomeResumido) {
        this.nomeResumido = nomeResumido;
        return this;
    }

    public void setNomeResumido(String nomeResumido) {
        this.nomeResumido = nomeResumido;
    }

    public String getEmail() {
        return email;
    }

    public Credenciamento email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFuncao() {
        return funcao;
    }

    public Credenciamento funcao(String funcao) {
        this.funcao = funcao;
        return this;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public StatusCredenciamento getStatusCredenciamento() {
        return statusCredenciamento;
    }

    public Credenciamento statusCredenciamento(StatusCredenciamento statusCredenciamento) {
        this.statusCredenciamento = statusCredenciamento;
        return this;
    }

    public void setStatusCredenciamento(StatusCredenciamento statusCredenciamento) {
        this.statusCredenciamento = statusCredenciamento;
    }

    public String getCep() {
        return cep;
    }

    public Credenciamento cep(String cep) {
        this.cep = cep;
        return this;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getRua() {
        return rua;
    }

    public Credenciamento rua(String rua) {
        this.rua = rua;
        return this;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumero() {
        return numero;
    }

    public Credenciamento numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public Credenciamento complemento(String complemento) {
        this.complemento = complemento;
        return this;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCidade() {
        return cidade;
    }

    public Credenciamento cidade(String cidade) {
        this.cidade = cidade;
        return this;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public Credenciamento uf(String uf) {
        this.uf = uf;
        return this;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getTelefoneResidencial() {
        return telefoneResidencial;
    }

    public Credenciamento telefoneResidencial(String telefoneResidencial) {
        this.telefoneResidencial = telefoneResidencial;
        return this;
    }

    public void setTelefoneResidencial(String telefoneResidencial) {
        this.telefoneResidencial = telefoneResidencial;
    }

    public String getCelular() {
        return celular;
    }

    public Credenciamento celular(String celular) {
        this.celular = celular;
        return this;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getWhatsApp() {
        return whatsApp;
    }

    public Credenciamento whatsApp(String whatsApp) {
        this.whatsApp = whatsApp;
        return this;
    }

    public void setWhatsApp(String whatsApp) {
        this.whatsApp = whatsApp;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public Credenciamento nomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
        return this;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getCnpj() {
        return cnpj;
    }

    public Credenciamento cnpj(String cnpj) {
        this.cnpj = cnpj;
        return this;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCepEmpresa() {
        return cepEmpresa;
    }

    public Credenciamento cepEmpresa(String cepEmpresa) {
        this.cepEmpresa = cepEmpresa;
        return this;
    }

    public void setCepEmpresa(String cepEmpresa) {
        this.cepEmpresa = cepEmpresa;
    }

    public String getEnderecoSede() {
        return enderecoSede;
    }

    public Credenciamento enderecoSede(String enderecoSede) {
        this.enderecoSede = enderecoSede;
        return this;
    }

    public void setEnderecoSede(String enderecoSede) {
        this.enderecoSede = enderecoSede;
    }

    public String getTelefoneSede() {
        return telefoneSede;
    }

    public Credenciamento telefoneSede(String telefoneSede) {
        this.telefoneSede = telefoneSede;
        return this;
    }

    public void setTelefoneSede(String telefoneSede) {
        this.telefoneSede = telefoneSede;
    }

    public TipoEmpresa getTipoEmpresa() {
        return tipoEmpresa;
    }

    public Credenciamento tipoEmpresa(TipoEmpresa tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
        return this;
    }

    public void setTipoEmpresa(TipoEmpresa tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    public String getEmailInstitucional() {
        return emailInstitucional;
    }

    public Credenciamento emailInstitucional(String emailInstitucional) {
        this.emailInstitucional = emailInstitucional;
        return this;
    }

    public void setEmailInstitucional(String emailInstitucional) {
        this.emailInstitucional = emailInstitucional;
    }

    public String getGerenteSetorOuProfissionalResponsavel() {
        return gerenteSetorOuProfissionalResponsavel;
    }

    public Credenciamento gerenteSetorOuProfissionalResponsavel(String gerenteSetorOuProfissionalResponsavel) {
        this.gerenteSetorOuProfissionalResponsavel = gerenteSetorOuProfissionalResponsavel;
        return this;
    }

    public void setGerenteSetorOuProfissionalResponsavel(String gerenteSetorOuProfissionalResponsavel) {
        this.gerenteSetorOuProfissionalResponsavel = gerenteSetorOuProfissionalResponsavel;
    }

    public String getTelefone() {
        return telefone;
    }

    public Credenciamento telefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getIdentidade() {
        return identidade;
    }

    public Credenciamento identidade(String identidade) {
        this.identidade = identidade;
        return this;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public String getOrgaoEmissor() {
        return orgaoEmissor;
    }

    public Credenciamento orgaoEmissor(String orgaoEmissor) {
        this.orgaoEmissor = orgaoEmissor;
        return this;
    }

    public void setOrgaoEmissor(String orgaoEmissor) {
        this.orgaoEmissor = orgaoEmissor;
    }

    public String getCpf() {
        return cpf;
    }

    public Credenciamento cpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDrt() {
        return drt;
    }

    public Credenciamento drt(String drt) {
        this.drt = drt;
        return this;
    }

    public void setDrt(String drt) {
        this.drt = drt;
    }

    public byte[] getFoto() {
        return foto;
    }

    public Credenciamento foto(byte[] foto) {
        this.foto = foto;
        return this;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getFotoContentType() {
        return fotoContentType;
    }

    public Credenciamento fotoContentType(String fotoContentType) {
        this.fotoContentType = fotoContentType;
        return this;
    }

    public void setFotoContentType(String fotoContentType) {
        this.fotoContentType = fotoContentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Credenciamento credenciamento = (Credenciamento) o;
        if (credenciamento.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, credenciamento.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Credenciamento{" +
            "id=" + id +
            ", nomeCompleto='" + nomeCompleto + "'" +
            ", nomeResumido='" + nomeResumido + "'" +
            ", email='" + email + "'" +
            ", funcao='" + funcao + "'" +
            ", statusCredenciamento='" + statusCredenciamento + "'" +
            ", cep='" + cep + "'" +
            ", rua='" + rua + "'" +
            ", numero='" + numero + "'" +
            ", complemento='" + complemento + "'" +
            ", cidade='" + cidade + "'" +
            ", uf='" + uf + "'" +
            ", telefoneResidencial='" + telefoneResidencial + "'" +
            ", celular='" + celular + "'" +
            ", whatsApp='" + whatsApp + "'" +
            ", nomeEmpresa='" + nomeEmpresa + "'" +
            ", cnpj='" + cnpj + "'" +
            ", cepEmpresa='" + cepEmpresa + "'" +
            ", enderecoSede='" + enderecoSede + "'" +
            ", telefoneSede='" + telefoneSede + "'" +
            ", tipoEmpresa='" + tipoEmpresa + "'" +
            ", emailInstitucional='" + emailInstitucional + "'" +
            ", gerenteSetorOuProfissionalResponsavel='" + gerenteSetorOuProfissionalResponsavel + "'" +
            ", telefone='" + telefone + "'" +
            ", identidade='" + identidade + "'" +
            ", orgaoEmissor='" + orgaoEmissor + "'" +
            ", cpf='" + cpf + "'" +
            ", drt='" + drt + "'" +
            ", foto='" + foto + "'" +
            ", fotoContentType='" + fotoContentType + "'" +
            '}';
    }
}
