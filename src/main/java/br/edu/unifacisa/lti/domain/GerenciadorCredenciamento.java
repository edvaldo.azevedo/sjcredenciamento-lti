package br.edu.unifacisa.lti.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A GerenciadorCredenciamento.
 */
@Entity
@Table(name = "gerenciador_credenciamento")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class GerenciadorCredenciamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GerenciadorCredenciamento gerenciadorCredenciamento = (GerenciadorCredenciamento) o;
        if (gerenciadorCredenciamento.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, gerenciadorCredenciamento.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "GerenciadorCredenciamento{" +
            "id=" + id +
            '}';
    }
}
