package br.edu.unifacisa.lti.domain.enumeration;

/**
 * The TipoEmpresa enumeration.
 */
public enum TipoEmpresa {
    VEICULO_DE_INTERNET,EMISSORA_DE_TELEVISAO,ESTACAO_DE_RADIO,JORNAL_IMPRESSO,REVISTA_IMPRESA,ORGAO_INSTITUCIONAL_GOVERNO
}
