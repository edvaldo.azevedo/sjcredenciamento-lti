package br.edu.unifacisa.lti.repository;

import br.edu.unifacisa.lti.domain.Credenciamento;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Credenciamento entity.
 */
@SuppressWarnings("unused")
public interface CredenciamentoRepository extends JpaRepository<Credenciamento,Long> {

}
