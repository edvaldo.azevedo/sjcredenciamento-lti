package br.edu.unifacisa.lti.repository;

import br.edu.unifacisa.lti.domain.GerenciadorCredenciamento;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the GerenciadorCredenciamento entity.
 */
@SuppressWarnings("unused")
public interface GerenciadorCredenciamentoRepository extends JpaRepository<GerenciadorCredenciamento,Long> {

}
