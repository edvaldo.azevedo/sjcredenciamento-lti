/**
 * View Models used by Spring MVC REST controllers.
 */
package br.edu.unifacisa.lti.web.rest.vm;
