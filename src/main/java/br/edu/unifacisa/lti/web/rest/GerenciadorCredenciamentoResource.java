package br.edu.unifacisa.lti.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import br.edu.unifacisa.lti.domain.GerenciadorCredenciamento;
import br.edu.unifacisa.lti.repository.GerenciadorCredenciamentoRepository;
import br.edu.unifacisa.lti.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing GerenciadorCredenciamento.
 */
@RestController
@RequestMapping("/api")
public class GerenciadorCredenciamentoResource {

    private final Logger log = LoggerFactory.getLogger(GerenciadorCredenciamentoResource.class);

    private static final String ENTITY_NAME = "gerenciadorCredenciamento";
        
    private final GerenciadorCredenciamentoRepository gerenciadorCredenciamentoRepository;

    public GerenciadorCredenciamentoResource(GerenciadorCredenciamentoRepository gerenciadorCredenciamentoRepository) {
        this.gerenciadorCredenciamentoRepository = gerenciadorCredenciamentoRepository;
    }

    /**
     * POST  /gerenciador-credenciamentos : Create a new gerenciadorCredenciamento.
     *
     * @param gerenciadorCredenciamento the gerenciadorCredenciamento to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gerenciadorCredenciamento, or with status 400 (Bad Request) if the gerenciadorCredenciamento has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gerenciador-credenciamentos")
    @Timed
    public ResponseEntity<GerenciadorCredenciamento> createGerenciadorCredenciamento(@RequestBody GerenciadorCredenciamento gerenciadorCredenciamento) throws URISyntaxException {
        log.debug("REST request to save GerenciadorCredenciamento : {}", gerenciadorCredenciamento);
        if (gerenciadorCredenciamento.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new gerenciadorCredenciamento cannot already have an ID")).body(null);
        }
        GerenciadorCredenciamento result = gerenciadorCredenciamentoRepository.save(gerenciadorCredenciamento);
        return ResponseEntity.created(new URI("/api/gerenciador-credenciamentos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /gerenciador-credenciamentos : Updates an existing gerenciadorCredenciamento.
     *
     * @param gerenciadorCredenciamento the gerenciadorCredenciamento to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gerenciadorCredenciamento,
     * or with status 400 (Bad Request) if the gerenciadorCredenciamento is not valid,
     * or with status 500 (Internal Server Error) if the gerenciadorCredenciamento couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/gerenciador-credenciamentos")
    @Timed
    public ResponseEntity<GerenciadorCredenciamento> updateGerenciadorCredenciamento(@RequestBody GerenciadorCredenciamento gerenciadorCredenciamento) throws URISyntaxException {
        log.debug("REST request to update GerenciadorCredenciamento : {}", gerenciadorCredenciamento);
        if (gerenciadorCredenciamento.getId() == null) {
            return createGerenciadorCredenciamento(gerenciadorCredenciamento);
        }
        GerenciadorCredenciamento result = gerenciadorCredenciamentoRepository.save(gerenciadorCredenciamento);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, gerenciadorCredenciamento.getId().toString()))
            .body(result);
    }

    /**
     * GET  /gerenciador-credenciamentos : get all the gerenciadorCredenciamentos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of gerenciadorCredenciamentos in body
     */
    @GetMapping("/gerenciador-credenciamentos")
    @Timed
    public List<GerenciadorCredenciamento> getAllGerenciadorCredenciamentos() {
        log.debug("REST request to get all GerenciadorCredenciamentos");
        List<GerenciadorCredenciamento> gerenciadorCredenciamentos = gerenciadorCredenciamentoRepository.findAll();
        return gerenciadorCredenciamentos;
    }

    /**
     * GET  /gerenciador-credenciamentos/:id : get the "id" gerenciadorCredenciamento.
     *
     * @param id the id of the gerenciadorCredenciamento to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gerenciadorCredenciamento, or with status 404 (Not Found)
     */
    @GetMapping("/gerenciador-credenciamentos/{id}")
    @Timed
    public ResponseEntity<GerenciadorCredenciamento> getGerenciadorCredenciamento(@PathVariable Long id) {
        log.debug("REST request to get GerenciadorCredenciamento : {}", id);
        GerenciadorCredenciamento gerenciadorCredenciamento = gerenciadorCredenciamentoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(gerenciadorCredenciamento));
    }

    /**
     * DELETE  /gerenciador-credenciamentos/:id : delete the "id" gerenciadorCredenciamento.
     *
     * @param id the id of the gerenciadorCredenciamento to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/gerenciador-credenciamentos/{id}")
    @Timed
    public ResponseEntity<Void> deleteGerenciadorCredenciamento(@PathVariable Long id) {
        log.debug("REST request to delete GerenciadorCredenciamento : {}", id);
        gerenciadorCredenciamentoRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
