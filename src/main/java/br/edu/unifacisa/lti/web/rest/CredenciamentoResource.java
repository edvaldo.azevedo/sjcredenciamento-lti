package br.edu.unifacisa.lti.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import br.edu.unifacisa.lti.domain.Credenciamento;
import br.edu.unifacisa.lti.domain.EnviadorEmail;
import br.edu.unifacisa.lti.domain.enumeration.StatusCredenciamento;
import br.edu.unifacisa.lti.repository.CredenciamentoRepository;
import br.edu.unifacisa.lti.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;





/**
 * REST controller for managing Credenciamento.
 */
@RestController
@RequestMapping("/api")
public class CredenciamentoResource {

    private final Logger log = LoggerFactory.getLogger(CredenciamentoResource.class);

    private static final String ENTITY_NAME = "credenciamento";
    private static final String ASSUNTO = "Credenciamento do Maior São João do Mundo 2017";
    private static final String ANALISE = "Sua solicitação de credenciamento "
    		+ "foi recebida, após análise dos dados informaremos a aprovação da credencial.\n \n"
    		+ "Coordenação de Comunicação.\n "
    		+ "O Maior São João do Mundo 2017. \n"
    		+ "Prefeitura Municipal de Campina Grande, PB.";
    private static final String APROVADO = "Sua credencial foi aprovada! \n "
    		+ "Data da retirada: 02 de junho de 2017 \n Local: Centro Cultural. "
    		+ "Lourdes Ramalho, rua Paulino Raposo, S/N, São José, Campina Grande - PB.\n \n "
    		+ "Coordenação de Comunicação.\n "
    		+ " O Maior São João do Mundo 2017. \n "
    		+ " Prefeitura Municipal de Campina Grande, PB.";
        
    private final CredenciamentoRepository credenciamentoRepository;
    private EnviadorEmail  email = new EnviadorEmail();

    public CredenciamentoResource(CredenciamentoRepository credenciamentoRepository) {
        this.credenciamentoRepository = credenciamentoRepository;
    }

    /**
     * POST  /credenciamentos : Create a new credenciamento.
     *
     * @param credenciamento the credenciamento to create
     * @return the ResponseEntity with status 201 (Created) and with body the new credenciamento, or with status 400 (Bad Request) if the credenciamento has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws GeneralSecurityException 
     */
    @PostMapping("/credenciamentos")
    @Timed
    public ResponseEntity<Credenciamento> createCredenciamento(@Valid @RequestBody Credenciamento credenciamento) throws URISyntaxException, GeneralSecurityException {
        log.debug("REST request to save Credenciamento : {}", credenciamento);
        if (credenciamento.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new credenciamento cannot already have an ID")).body(null);
        }
        credenciamento.setStatusCredenciamento(StatusCredenciamento.EM_ANALISE);
        email.send(credenciamento.getEmail(), ASSUNTO,"Olá,"+" " +credenciamento.getNomeResumido()+":"+"\n"+ ANALISE);

        Credenciamento result = credenciamentoRepository.save(credenciamento);
        return ResponseEntity.created(new URI("/api/credenciamentos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /credenciamentos : Updates an existing credenciamento.
     *
     * @param credenciamento the credenciamento to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated credenciamento,
     * or with status 400 (Bad Request) if the credenciamento is not valid,
     * or with status 500 (Internal Server Error) if the credenciamento couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws GeneralSecurityException 
     */
    @PutMapping("/credenciamentos")
    @Timed
    public ResponseEntity<Credenciamento> updateCredenciamento(@Valid @RequestBody Credenciamento credenciamento) throws URISyntaxException, GeneralSecurityException {
        log.debug("REST request to update Credenciamento : {}", credenciamento);
        if (credenciamento.getId() == null) {
            return createCredenciamento(credenciamento);
        }
        if(credenciamento.getStatusCredenciamento().equals(StatusCredenciamento.APROVADO)){
        	
        	email.send(credenciamento.getEmail(), ASSUNTO,"Olá," +credenciamento.getNomeResumido()+":"+"\n"+ APROVADO);
        }else{
        	
        }
        Credenciamento result = credenciamentoRepository.save(credenciamento);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, credenciamento.getId().toString()))
            .body(result);
    }

    /**
     * GET  /credenciamentos : get all the credenciamentos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of credenciamentos in body
     */
    @GetMapping("/credenciamentos")
    @Timed
    public List<Credenciamento> getAllCredenciamentos() {
        log.debug("REST request to get all Credenciamentos");
        List<Credenciamento> credenciamentos = credenciamentoRepository.findAll();
        return credenciamentos;
    }

    /**
     * GET  /credenciamentos/:id : get the "id" credenciamento.
     *
     * @param id the id of the credenciamento to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the credenciamento, or with status 404 (Not Found)
     */
    @GetMapping("/credenciamentos/{id}")
    @Timed
    public ResponseEntity<Credenciamento> getCredenciamento(@PathVariable Long id) {
        log.debug("REST request to get Credenciamento : {}", id);
        Credenciamento credenciamento = credenciamentoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(credenciamento));
    }

    /**
     * DELETE  /credenciamentos/:id : delete the "id" credenciamento.
     *
     * @param id the id of the credenciamento to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/credenciamentos/{id}")
    @Timed
    public ResponseEntity<Void> deleteCredenciamento(@PathVariable Long id) {
        log.debug("REST request to delete Credenciamento : {}", id);
        credenciamentoRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
