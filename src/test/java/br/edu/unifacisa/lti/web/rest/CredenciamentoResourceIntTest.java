package br.edu.unifacisa.lti.web.rest;

import br.edu.unifacisa.lti.CredenciamentoApp;

import br.edu.unifacisa.lti.domain.Credenciamento;
import br.edu.unifacisa.lti.repository.CredenciamentoRepository;
import br.edu.unifacisa.lti.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.edu.unifacisa.lti.domain.enumeration.StatusCredenciamento;
import br.edu.unifacisa.lti.domain.enumeration.TipoEmpresa;
/**
 * Test class for the CredenciamentoResource REST controller.
 *
 * @see CredenciamentoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CredenciamentoApp.class)
public class CredenciamentoResourceIntTest {

    private static final String DEFAULT_NOME_COMPLETO = "AAAAAAAAAA";
    private static final String UPDATED_NOME_COMPLETO = "BBBBBBBBBB";

    private static final String DEFAULT_NOME_RESUMIDO = "AAAAAAAAAA";
    private static final String UPDATED_NOME_RESUMIDO = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_FUNCAO = "AAAAAAAAAA";
    private static final String UPDATED_FUNCAO = "BBBBBBBBBB";

    private static final StatusCredenciamento DEFAULT_STATUS_CREDENCIAMENTO = StatusCredenciamento.EM_ANALISE;
    private static final StatusCredenciamento UPDATED_STATUS_CREDENCIAMENTO = StatusCredenciamento.APROVADO;

    private static final String DEFAULT_CEP = "AAAAAAAAAA";
    private static final String UPDATED_CEP = "BBBBBBBBBB";

    private static final String DEFAULT_RUA = "AAAAAAAAAA";
    private static final String UPDATED_RUA = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO = "BBBBBBBBBB";

    private static final String DEFAULT_COMPLEMENTO = "AAAAAAAAAA";
    private static final String UPDATED_COMPLEMENTO = "BBBBBBBBBB";

    private static final String DEFAULT_CIDADE = "AAAAAAAAAA";
    private static final String UPDATED_CIDADE = "BBBBBBBBBB";

    private static final String DEFAULT_UF = "AAAAAAAAAA";
    private static final String UPDATED_UF = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONE_RESIDENCIAL = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONE_RESIDENCIAL = "BBBBBBBBBB";

    private static final String DEFAULT_CELULAR = "AAAAAAAAAA";
    private static final String UPDATED_CELULAR = "BBBBBBBBBB";

    private static final String DEFAULT_WHATS_APP = "AAAAAAAAAA";
    private static final String UPDATED_WHATS_APP = "BBBBBBBBBB";

    private static final String DEFAULT_NOME_EMPRESA = "AAAAAAAAAA";
    private static final String UPDATED_NOME_EMPRESA = "BBBBBBBBBB";

    private static final String DEFAULT_CNPJ = "AAAAAAAAAA";
    private static final String UPDATED_CNPJ = "BBBBBBBBBB";

    private static final String DEFAULT_CEP_EMPRESA = "AAAAAAAAAA";
    private static final String UPDATED_CEP_EMPRESA = "BBBBBBBBBB";

    private static final String DEFAULT_ENDERECO_SEDE = "AAAAAAAAAA";
    private static final String UPDATED_ENDERECO_SEDE = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONE_SEDE = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONE_SEDE = "BBBBBBBBBB";

    private static final TipoEmpresa DEFAULT_TIPO_EMPRESA = TipoEmpresa.VEICULO_DE_INTERNET;
    private static final TipoEmpresa UPDATED_TIPO_EMPRESA = TipoEmpresa.EMISSORA_DE_TELEVISAO;

    private static final String DEFAULT_EMAIL_INSTITUCIONAL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_INSTITUCIONAL = "BBBBBBBBBB";

    private static final String DEFAULT_GERENTE_SETOR_OU_PROFISSIONAL_RESPONSAVEL = "AAAAAAAAAA";
    private static final String UPDATED_GERENTE_SETOR_OU_PROFISSIONAL_RESPONSAVEL = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONE = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIDADE = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIDADE = "BBBBBBBBBB";

    private static final String DEFAULT_ORGAO_EMISSOR = "AAAAAAAAAA";
    private static final String UPDATED_ORGAO_EMISSOR = "BBBBBBBBBB";

    private static final String DEFAULT_CPF = "AAAAAAAAAA";
    private static final String UPDATED_CPF = "BBBBBBBBBB";

    private static final String DEFAULT_DRT = "AAAAAAAAAA";
    private static final String UPDATED_DRT = "BBBBBBBBBB";

    private static final byte[] DEFAULT_FOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FOTO = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_FOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FOTO_CONTENT_TYPE = "image/png";

    @Autowired
    private CredenciamentoRepository credenciamentoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCredenciamentoMockMvc;

    private Credenciamento credenciamento;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CredenciamentoResource credenciamentoResource = new CredenciamentoResource(credenciamentoRepository);
        this.restCredenciamentoMockMvc = MockMvcBuilders.standaloneSetup(credenciamentoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Credenciamento createEntity(EntityManager em) {
        Credenciamento credenciamento = new Credenciamento()
            .nomeCompleto(DEFAULT_NOME_COMPLETO)
            .nomeResumido(DEFAULT_NOME_RESUMIDO)
            .email(DEFAULT_EMAIL)
            .funcao(DEFAULT_FUNCAO)
            .statusCredenciamento(DEFAULT_STATUS_CREDENCIAMENTO)
            .cep(DEFAULT_CEP)
            .rua(DEFAULT_RUA)
            .numero(DEFAULT_NUMERO)
            .complemento(DEFAULT_COMPLEMENTO)
            .cidade(DEFAULT_CIDADE)
            .uf(DEFAULT_UF)
            .telefoneResidencial(DEFAULT_TELEFONE_RESIDENCIAL)
            .celular(DEFAULT_CELULAR)
            .whatsApp(DEFAULT_WHATS_APP)
            .nomeEmpresa(DEFAULT_NOME_EMPRESA)
            .cnpj(DEFAULT_CNPJ)
            .cepEmpresa(DEFAULT_CEP_EMPRESA)
            .enderecoSede(DEFAULT_ENDERECO_SEDE)
            .telefoneSede(DEFAULT_TELEFONE_SEDE)
            .tipoEmpresa(DEFAULT_TIPO_EMPRESA)
            .emailInstitucional(DEFAULT_EMAIL_INSTITUCIONAL)
            .gerenteSetorOuProfissionalResponsavel(DEFAULT_GERENTE_SETOR_OU_PROFISSIONAL_RESPONSAVEL)
            .telefone(DEFAULT_TELEFONE)
            .identidade(DEFAULT_IDENTIDADE)
            .orgaoEmissor(DEFAULT_ORGAO_EMISSOR)
            .cpf(DEFAULT_CPF)
            .drt(DEFAULT_DRT)
            .foto(DEFAULT_FOTO)
            .fotoContentType(DEFAULT_FOTO_CONTENT_TYPE);
        return credenciamento;
    }

    @Before
    public void initTest() {
        credenciamento = createEntity(em);
    }

    @Test
    @Transactional
    public void createCredenciamento() throws Exception {
        int databaseSizeBeforeCreate = credenciamentoRepository.findAll().size();

        // Create the Credenciamento
        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isCreated());

        // Validate the Credenciamento in the database
        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeCreate + 1);
        Credenciamento testCredenciamento = credenciamentoList.get(credenciamentoList.size() - 1);
        assertThat(testCredenciamento.getNomeCompleto()).isEqualTo(DEFAULT_NOME_COMPLETO);
        assertThat(testCredenciamento.getNomeResumido()).isEqualTo(DEFAULT_NOME_RESUMIDO);
        assertThat(testCredenciamento.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCredenciamento.getFuncao()).isEqualTo(DEFAULT_FUNCAO);
        assertThat(testCredenciamento.getStatusCredenciamento()).isEqualTo(DEFAULT_STATUS_CREDENCIAMENTO);
        assertThat(testCredenciamento.getCep()).isEqualTo(DEFAULT_CEP);
        assertThat(testCredenciamento.getRua()).isEqualTo(DEFAULT_RUA);
        assertThat(testCredenciamento.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testCredenciamento.getComplemento()).isEqualTo(DEFAULT_COMPLEMENTO);
        assertThat(testCredenciamento.getCidade()).isEqualTo(DEFAULT_CIDADE);
        assertThat(testCredenciamento.getUf()).isEqualTo(DEFAULT_UF);
        assertThat(testCredenciamento.getTelefoneResidencial()).isEqualTo(DEFAULT_TELEFONE_RESIDENCIAL);
        assertThat(testCredenciamento.getCelular()).isEqualTo(DEFAULT_CELULAR);
        assertThat(testCredenciamento.getWhatsApp()).isEqualTo(DEFAULT_WHATS_APP);
        assertThat(testCredenciamento.getNomeEmpresa()).isEqualTo(DEFAULT_NOME_EMPRESA);
        assertThat(testCredenciamento.getCnpj()).isEqualTo(DEFAULT_CNPJ);
        assertThat(testCredenciamento.getCepEmpresa()).isEqualTo(DEFAULT_CEP_EMPRESA);
        assertThat(testCredenciamento.getEnderecoSede()).isEqualTo(DEFAULT_ENDERECO_SEDE);
        assertThat(testCredenciamento.getTelefoneSede()).isEqualTo(DEFAULT_TELEFONE_SEDE);
        assertThat(testCredenciamento.getTipoEmpresa()).isEqualTo(DEFAULT_TIPO_EMPRESA);
        assertThat(testCredenciamento.getEmailInstitucional()).isEqualTo(DEFAULT_EMAIL_INSTITUCIONAL);
        assertThat(testCredenciamento.getGerenteSetorOuProfissionalResponsavel()).isEqualTo(DEFAULT_GERENTE_SETOR_OU_PROFISSIONAL_RESPONSAVEL);
        assertThat(testCredenciamento.getTelefone()).isEqualTo(DEFAULT_TELEFONE);
        assertThat(testCredenciamento.getIdentidade()).isEqualTo(DEFAULT_IDENTIDADE);
        assertThat(testCredenciamento.getOrgaoEmissor()).isEqualTo(DEFAULT_ORGAO_EMISSOR);
        assertThat(testCredenciamento.getCpf()).isEqualTo(DEFAULT_CPF);
        assertThat(testCredenciamento.getDrt()).isEqualTo(DEFAULT_DRT);
        assertThat(testCredenciamento.getFoto()).isEqualTo(DEFAULT_FOTO);
        assertThat(testCredenciamento.getFotoContentType()).isEqualTo(DEFAULT_FOTO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createCredenciamentoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = credenciamentoRepository.findAll().size();

        // Create the Credenciamento with an existing ID
        credenciamento.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNomeCompletoIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setNomeCompleto(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomeResumidoIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setNomeResumido(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setEmail(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFuncaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setFuncao(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCepIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setCep(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRuaIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setRua(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumeroIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setNumero(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCidadeIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setCidade(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUfIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setUf(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

   

    @Test
    @Transactional
    public void checkCelularIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setCelular(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    

    @Test
    @Transactional
    public void checkNomeEmpresaIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setNomeEmpresa(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCnpjIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setCnpj(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCepEmpresaIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setCepEmpresa(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnderecoSedeIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setEnderecoSede(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTelefoneSedeIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setTelefoneSede(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoEmpresaIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setTipoEmpresa(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailInstitucionalIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setEmailInstitucional(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGerenteSetorOuProfissionalResponsavelIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setGerenteSetorOuProfissionalResponsavel(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTelefoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setTelefone(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIdentidadeIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setIdentidade(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOrgaoEmissorIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setOrgaoEmissor(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCpfIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setCpf(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    

    @Test
    @Transactional
    public void checkFotoIsRequired() throws Exception {
        int databaseSizeBeforeTest = credenciamentoRepository.findAll().size();
        // set the field null
        credenciamento.setFoto(null);

        // Create the Credenciamento, which fails.

        restCredenciamentoMockMvc.perform(post("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isBadRequest());

        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCredenciamentos() throws Exception {
        // Initialize the database
        credenciamentoRepository.saveAndFlush(credenciamento);

        // Get all the credenciamentoList
        restCredenciamentoMockMvc.perform(get("/api/credenciamentos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(credenciamento.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeCompleto").value(hasItem(DEFAULT_NOME_COMPLETO.toString())))
            .andExpect(jsonPath("$.[*].nomeResumido").value(hasItem(DEFAULT_NOME_RESUMIDO.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].funcao").value(hasItem(DEFAULT_FUNCAO.toString())))
            .andExpect(jsonPath("$.[*].statusCredenciamento").value(hasItem(DEFAULT_STATUS_CREDENCIAMENTO.toString())))
            .andExpect(jsonPath("$.[*].cep").value(hasItem(DEFAULT_CEP.toString())))
            .andExpect(jsonPath("$.[*].rua").value(hasItem(DEFAULT_RUA.toString())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO.toString())))
            .andExpect(jsonPath("$.[*].complemento").value(hasItem(DEFAULT_COMPLEMENTO.toString())))
            .andExpect(jsonPath("$.[*].cidade").value(hasItem(DEFAULT_CIDADE.toString())))
            .andExpect(jsonPath("$.[*].uf").value(hasItem(DEFAULT_UF.toString())))
            .andExpect(jsonPath("$.[*].telefoneResidencial").value(hasItem(DEFAULT_TELEFONE_RESIDENCIAL.toString())))
            .andExpect(jsonPath("$.[*].celular").value(hasItem(DEFAULT_CELULAR.toString())))
            .andExpect(jsonPath("$.[*].whatsApp").value(hasItem(DEFAULT_WHATS_APP.toString())))
            .andExpect(jsonPath("$.[*].nomeEmpresa").value(hasItem(DEFAULT_NOME_EMPRESA.toString())))
            .andExpect(jsonPath("$.[*].cnpj").value(hasItem(DEFAULT_CNPJ.toString())))
            .andExpect(jsonPath("$.[*].cepEmpresa").value(hasItem(DEFAULT_CEP_EMPRESA.toString())))
            .andExpect(jsonPath("$.[*].enderecoSede").value(hasItem(DEFAULT_ENDERECO_SEDE.toString())))
            .andExpect(jsonPath("$.[*].telefoneSede").value(hasItem(DEFAULT_TELEFONE_SEDE.toString())))
            .andExpect(jsonPath("$.[*].tipoEmpresa").value(hasItem(DEFAULT_TIPO_EMPRESA.toString())))
            .andExpect(jsonPath("$.[*].emailInstitucional").value(hasItem(DEFAULT_EMAIL_INSTITUCIONAL.toString())))
            .andExpect(jsonPath("$.[*].gerenteSetorOuProfissionalResponsavel").value(hasItem(DEFAULT_GERENTE_SETOR_OU_PROFISSIONAL_RESPONSAVEL.toString())))
            .andExpect(jsonPath("$.[*].telefone").value(hasItem(DEFAULT_TELEFONE.toString())))
            .andExpect(jsonPath("$.[*].identidade").value(hasItem(DEFAULT_IDENTIDADE.toString())))
            .andExpect(jsonPath("$.[*].orgaoEmissor").value(hasItem(DEFAULT_ORGAO_EMISSOR.toString())))
            .andExpect(jsonPath("$.[*].cpf").value(hasItem(DEFAULT_CPF.toString())))
            .andExpect(jsonPath("$.[*].drt").value(hasItem(DEFAULT_DRT.toString())))
            .andExpect(jsonPath("$.[*].fotoContentType").value(hasItem(DEFAULT_FOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].foto").value(hasItem(Base64Utils.encodeToString(DEFAULT_FOTO))));
    }

    @Test
    @Transactional
    public void getCredenciamento() throws Exception {
        // Initialize the database
        credenciamentoRepository.saveAndFlush(credenciamento);

        // Get the credenciamento
        restCredenciamentoMockMvc.perform(get("/api/credenciamentos/{id}", credenciamento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(credenciamento.getId().intValue()))
            .andExpect(jsonPath("$.nomeCompleto").value(DEFAULT_NOME_COMPLETO.toString()))
            .andExpect(jsonPath("$.nomeResumido").value(DEFAULT_NOME_RESUMIDO.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.funcao").value(DEFAULT_FUNCAO.toString()))
            .andExpect(jsonPath("$.statusCredenciamento").value(DEFAULT_STATUS_CREDENCIAMENTO.toString()))
            .andExpect(jsonPath("$.cep").value(DEFAULT_CEP.toString()))
            .andExpect(jsonPath("$.rua").value(DEFAULT_RUA.toString()))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO.toString()))
            .andExpect(jsonPath("$.complemento").value(DEFAULT_COMPLEMENTO.toString()))
            .andExpect(jsonPath("$.cidade").value(DEFAULT_CIDADE.toString()))
            .andExpect(jsonPath("$.uf").value(DEFAULT_UF.toString()))
            .andExpect(jsonPath("$.telefoneResidencial").value(DEFAULT_TELEFONE_RESIDENCIAL.toString()))
            .andExpect(jsonPath("$.celular").value(DEFAULT_CELULAR.toString()))
            .andExpect(jsonPath("$.whatsApp").value(DEFAULT_WHATS_APP.toString()))
            .andExpect(jsonPath("$.nomeEmpresa").value(DEFAULT_NOME_EMPRESA.toString()))
            .andExpect(jsonPath("$.cnpj").value(DEFAULT_CNPJ.toString()))
            .andExpect(jsonPath("$.cepEmpresa").value(DEFAULT_CEP_EMPRESA.toString()))
            .andExpect(jsonPath("$.enderecoSede").value(DEFAULT_ENDERECO_SEDE.toString()))
            .andExpect(jsonPath("$.telefoneSede").value(DEFAULT_TELEFONE_SEDE.toString()))
            .andExpect(jsonPath("$.tipoEmpresa").value(DEFAULT_TIPO_EMPRESA.toString()))
            .andExpect(jsonPath("$.emailInstitucional").value(DEFAULT_EMAIL_INSTITUCIONAL.toString()))
            .andExpect(jsonPath("$.gerenteSetorOuProfissionalResponsavel").value(DEFAULT_GERENTE_SETOR_OU_PROFISSIONAL_RESPONSAVEL.toString()))
            .andExpect(jsonPath("$.telefone").value(DEFAULT_TELEFONE.toString()))
            .andExpect(jsonPath("$.identidade").value(DEFAULT_IDENTIDADE.toString()))
            .andExpect(jsonPath("$.orgaoEmissor").value(DEFAULT_ORGAO_EMISSOR.toString()))
            .andExpect(jsonPath("$.cpf").value(DEFAULT_CPF.toString()))
            .andExpect(jsonPath("$.drt").value(DEFAULT_DRT.toString()))
            .andExpect(jsonPath("$.fotoContentType").value(DEFAULT_FOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.foto").value(Base64Utils.encodeToString(DEFAULT_FOTO)));
    }

    @Test
    @Transactional
    public void getNonExistingCredenciamento() throws Exception {
        // Get the credenciamento
        restCredenciamentoMockMvc.perform(get("/api/credenciamentos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCredenciamento() throws Exception {
        // Initialize the database
        credenciamentoRepository.saveAndFlush(credenciamento);
        int databaseSizeBeforeUpdate = credenciamentoRepository.findAll().size();

        // Update the credenciamento
        Credenciamento updatedCredenciamento = credenciamentoRepository.findOne(credenciamento.getId());
        updatedCredenciamento
            .nomeCompleto(UPDATED_NOME_COMPLETO)
            .nomeResumido(UPDATED_NOME_RESUMIDO)
            .email(UPDATED_EMAIL)
            .funcao(UPDATED_FUNCAO)
            .statusCredenciamento(UPDATED_STATUS_CREDENCIAMENTO)
            .cep(UPDATED_CEP)
            .rua(UPDATED_RUA)
            .numero(UPDATED_NUMERO)
            .complemento(UPDATED_COMPLEMENTO)
            .cidade(UPDATED_CIDADE)
            .uf(UPDATED_UF)
            .telefoneResidencial(UPDATED_TELEFONE_RESIDENCIAL)
            .celular(UPDATED_CELULAR)
            .whatsApp(UPDATED_WHATS_APP)
            .nomeEmpresa(UPDATED_NOME_EMPRESA)
            .cnpj(UPDATED_CNPJ)
            .cepEmpresa(UPDATED_CEP_EMPRESA)
            .enderecoSede(UPDATED_ENDERECO_SEDE)
            .telefoneSede(UPDATED_TELEFONE_SEDE)
            .tipoEmpresa(UPDATED_TIPO_EMPRESA)
            .emailInstitucional(UPDATED_EMAIL_INSTITUCIONAL)
            .gerenteSetorOuProfissionalResponsavel(UPDATED_GERENTE_SETOR_OU_PROFISSIONAL_RESPONSAVEL)
            .telefone(UPDATED_TELEFONE)
            .identidade(UPDATED_IDENTIDADE)
            .orgaoEmissor(UPDATED_ORGAO_EMISSOR)
            .cpf(UPDATED_CPF)
            .drt(UPDATED_DRT)
            .foto(UPDATED_FOTO)
            .fotoContentType(UPDATED_FOTO_CONTENT_TYPE);

        restCredenciamentoMockMvc.perform(put("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCredenciamento)))
            .andExpect(status().isOk());

        // Validate the Credenciamento in the database
        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeUpdate);
        Credenciamento testCredenciamento = credenciamentoList.get(credenciamentoList.size() - 1);
        assertThat(testCredenciamento.getNomeCompleto()).isEqualTo(UPDATED_NOME_COMPLETO);
        assertThat(testCredenciamento.getNomeResumido()).isEqualTo(UPDATED_NOME_RESUMIDO);
        assertThat(testCredenciamento.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCredenciamento.getFuncao()).isEqualTo(UPDATED_FUNCAO);
        assertThat(testCredenciamento.getStatusCredenciamento()).isEqualTo(UPDATED_STATUS_CREDENCIAMENTO);
        assertThat(testCredenciamento.getCep()).isEqualTo(UPDATED_CEP);
        assertThat(testCredenciamento.getRua()).isEqualTo(UPDATED_RUA);
        assertThat(testCredenciamento.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testCredenciamento.getComplemento()).isEqualTo(UPDATED_COMPLEMENTO);
        assertThat(testCredenciamento.getCidade()).isEqualTo(UPDATED_CIDADE);
        assertThat(testCredenciamento.getUf()).isEqualTo(UPDATED_UF);
        assertThat(testCredenciamento.getTelefoneResidencial()).isEqualTo(UPDATED_TELEFONE_RESIDENCIAL);
        assertThat(testCredenciamento.getCelular()).isEqualTo(UPDATED_CELULAR);
        assertThat(testCredenciamento.getWhatsApp()).isEqualTo(UPDATED_WHATS_APP);
        assertThat(testCredenciamento.getNomeEmpresa()).isEqualTo(UPDATED_NOME_EMPRESA);
        assertThat(testCredenciamento.getCnpj()).isEqualTo(UPDATED_CNPJ);
        assertThat(testCredenciamento.getCepEmpresa()).isEqualTo(UPDATED_CEP_EMPRESA);
        assertThat(testCredenciamento.getEnderecoSede()).isEqualTo(UPDATED_ENDERECO_SEDE);
        assertThat(testCredenciamento.getTelefoneSede()).isEqualTo(UPDATED_TELEFONE_SEDE);
        assertThat(testCredenciamento.getTipoEmpresa()).isEqualTo(UPDATED_TIPO_EMPRESA);
        assertThat(testCredenciamento.getEmailInstitucional()).isEqualTo(UPDATED_EMAIL_INSTITUCIONAL);
        assertThat(testCredenciamento.getGerenteSetorOuProfissionalResponsavel()).isEqualTo(UPDATED_GERENTE_SETOR_OU_PROFISSIONAL_RESPONSAVEL);
        assertThat(testCredenciamento.getTelefone()).isEqualTo(UPDATED_TELEFONE);
        assertThat(testCredenciamento.getIdentidade()).isEqualTo(UPDATED_IDENTIDADE);
        assertThat(testCredenciamento.getOrgaoEmissor()).isEqualTo(UPDATED_ORGAO_EMISSOR);
        assertThat(testCredenciamento.getCpf()).isEqualTo(UPDATED_CPF);
        assertThat(testCredenciamento.getDrt()).isEqualTo(UPDATED_DRT);
        assertThat(testCredenciamento.getFoto()).isEqualTo(UPDATED_FOTO);
        assertThat(testCredenciamento.getFotoContentType()).isEqualTo(UPDATED_FOTO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingCredenciamento() throws Exception {
        int databaseSizeBeforeUpdate = credenciamentoRepository.findAll().size();

        // Create the Credenciamento

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCredenciamentoMockMvc.perform(put("/api/credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(credenciamento)))
            .andExpect(status().isCreated());

        // Validate the Credenciamento in the database
        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCredenciamento() throws Exception {
        // Initialize the database
        credenciamentoRepository.saveAndFlush(credenciamento);
        int databaseSizeBeforeDelete = credenciamentoRepository.findAll().size();

        // Get the credenciamento
        restCredenciamentoMockMvc.perform(delete("/api/credenciamentos/{id}", credenciamento.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Credenciamento> credenciamentoList = credenciamentoRepository.findAll();
        assertThat(credenciamentoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Credenciamento.class);
    }
}
