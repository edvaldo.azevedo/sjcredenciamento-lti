package br.edu.unifacisa.lti.web.rest;



import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import br.edu.unifacisa.lti.CredenciamentoApp;
import br.edu.unifacisa.lti.domain.GerenciadorCredenciamento;
import br.edu.unifacisa.lti.repository.GerenciadorCredenciamentoRepository;
import br.edu.unifacisa.lti.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the GerenciadorCredenciamentoResource REST controller.
 *
 * @see GerenciadorCredenciamentoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CredenciamentoApp.class)
public class GerenciadorCredenciamentoResourceIntTest {

    @Autowired
    private GerenciadorCredenciamentoRepository gerenciadorCredenciamentoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restGerenciadorCredenciamentoMockMvc;

    private GerenciadorCredenciamento gerenciadorCredenciamento;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GerenciadorCredenciamentoResource gerenciadorCredenciamentoResource = new GerenciadorCredenciamentoResource(gerenciadorCredenciamentoRepository);
        this.restGerenciadorCredenciamentoMockMvc = MockMvcBuilders.standaloneSetup(gerenciadorCredenciamentoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GerenciadorCredenciamento createEntity(EntityManager em) {
        GerenciadorCredenciamento gerenciadorCredenciamento = new GerenciadorCredenciamento();
        return gerenciadorCredenciamento;
    }

    @Before
    public void initTest() {
        gerenciadorCredenciamento = createEntity(em);
    }

    @Test
    @Transactional
    public void createGerenciadorCredenciamento() throws Exception {
        int databaseSizeBeforeCreate = gerenciadorCredenciamentoRepository.findAll().size();

        // Create the GerenciadorCredenciamento
        restGerenciadorCredenciamentoMockMvc.perform(post("/api/gerenciador-credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gerenciadorCredenciamento)))
            .andExpect(status().isCreated());

        // Validate the GerenciadorCredenciamento in the database
        List<GerenciadorCredenciamento> gerenciadorCredenciamentoList = gerenciadorCredenciamentoRepository.findAll();
        assertThat(gerenciadorCredenciamentoList).hasSize(databaseSizeBeforeCreate + 1);
        GerenciadorCredenciamento testGerenciadorCredenciamento = gerenciadorCredenciamentoList.get(gerenciadorCredenciamentoList.size() - 1);
    }

    @Test
    @Transactional
    public void createGerenciadorCredenciamentoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = gerenciadorCredenciamentoRepository.findAll().size();

        // Create the GerenciadorCredenciamento with an existing ID
        gerenciadorCredenciamento.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGerenciadorCredenciamentoMockMvc.perform(post("/api/gerenciador-credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gerenciadorCredenciamento)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<GerenciadorCredenciamento> gerenciadorCredenciamentoList = gerenciadorCredenciamentoRepository.findAll();
        assertThat(gerenciadorCredenciamentoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllGerenciadorCredenciamentos() throws Exception {
        // Initialize the database
        gerenciadorCredenciamentoRepository.saveAndFlush(gerenciadorCredenciamento);

        // Get all the gerenciadorCredenciamentoList
        restGerenciadorCredenciamentoMockMvc.perform(get("/api/gerenciador-credenciamentos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gerenciadorCredenciamento.getId().intValue())));
    }

    @Test
    @Transactional
    public void getGerenciadorCredenciamento() throws Exception {
        // Initialize the database
        gerenciadorCredenciamentoRepository.saveAndFlush(gerenciadorCredenciamento);

        // Get the gerenciadorCredenciamento
        restGerenciadorCredenciamentoMockMvc.perform(get("/api/gerenciador-credenciamentos/{id}", gerenciadorCredenciamento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(gerenciadorCredenciamento.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingGerenciadorCredenciamento() throws Exception {
        // Get the gerenciadorCredenciamento
        restGerenciadorCredenciamentoMockMvc.perform(get("/api/gerenciador-credenciamentos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGerenciadorCredenciamento() throws Exception {
        // Initialize the database
        gerenciadorCredenciamentoRepository.saveAndFlush(gerenciadorCredenciamento);
        int databaseSizeBeforeUpdate = gerenciadorCredenciamentoRepository.findAll().size();

        // Update the gerenciadorCredenciamento
        GerenciadorCredenciamento updatedGerenciadorCredenciamento = gerenciadorCredenciamentoRepository.findOne(gerenciadorCredenciamento.getId());

        restGerenciadorCredenciamentoMockMvc.perform(put("/api/gerenciador-credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedGerenciadorCredenciamento)))
            .andExpect(status().isOk());

        // Validate the GerenciadorCredenciamento in the database
        List<GerenciadorCredenciamento> gerenciadorCredenciamentoList = gerenciadorCredenciamentoRepository.findAll();
        assertThat(gerenciadorCredenciamentoList).hasSize(databaseSizeBeforeUpdate);
        GerenciadorCredenciamento testGerenciadorCredenciamento = gerenciadorCredenciamentoList.get(gerenciadorCredenciamentoList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingGerenciadorCredenciamento() throws Exception {
        int databaseSizeBeforeUpdate = gerenciadorCredenciamentoRepository.findAll().size();

        // Create the GerenciadorCredenciamento

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restGerenciadorCredenciamentoMockMvc.perform(put("/api/gerenciador-credenciamentos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gerenciadorCredenciamento)))
            .andExpect(status().isCreated());

        // Validate the GerenciadorCredenciamento in the database
        List<GerenciadorCredenciamento> gerenciadorCredenciamentoList = gerenciadorCredenciamentoRepository.findAll();
        assertThat(gerenciadorCredenciamentoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteGerenciadorCredenciamento() throws Exception {
        // Initialize the database
        gerenciadorCredenciamentoRepository.saveAndFlush(gerenciadorCredenciamento);
        int databaseSizeBeforeDelete = gerenciadorCredenciamentoRepository.findAll().size();

        // Get the gerenciadorCredenciamento
        restGerenciadorCredenciamentoMockMvc.perform(delete("/api/gerenciador-credenciamentos/{id}", gerenciadorCredenciamento.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<GerenciadorCredenciamento> gerenciadorCredenciamentoList = gerenciadorCredenciamentoRepository.findAll();
        assertThat(gerenciadorCredenciamentoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GerenciadorCredenciamento.class);
    }
}
